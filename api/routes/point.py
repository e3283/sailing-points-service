from fastapi import APIRouter, status

router = APIRouter(prefix="/points", tags=["points"])


@router.get("/", status_code=status.HTTP_200_OK)
def index():
    """Test route that will be removed."""
    return {"points": True}
