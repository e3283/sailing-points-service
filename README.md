# Sailing Points Service

## Context

In a sailing competion multiple races are conducted over a number of different days. When a sailor finishes a race by crossing the finish line they are awarded a number of points. At the end of the sailing competition the sailor with the least ammount of points is announced the winner.

## Pointing System

The points are awarded as follows:

The finishing order determines the pointes awarded. The first sailor to cross the line is awarded one point, the second two and so on until the final sailor crosses the line.

In some cases penalties can be issued. Each penalty coresponds to a number of points.
